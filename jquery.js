$(function(){
    // alert("The document is ready!");
    $('#a1').on("click", function(){
        $(this).addClass = "classDown";
    });
    $('#a1').on("click", function(){
        $(this).hide("fast");
    });
    $("#a2").on("click", function(){
        $("#a1").show("fast");
    });

    $("#btnAdd").on("click", function(){
        var newElement;
        for(var i=3; i<11; i++){
            newElement = $("<li><a href=\"#\">New Link_" + i + "</a></li>");
            $("ul").append(newElement);
        }
    });
    $("#btnColor").on("click", function(){
        $("a").css("color", "red");
        $("a").css("text-decoration", "line-through");
        $("#secondList").delay(1000).hide("fast");
    });
    // remove even elements from the list
    $("#btnList").on("click", function(){
        $("#secondList li:even").remove();
        var liSecondList = $("#secondList li");

        var lengthList = liSecondList.length;
        var messaggio;
        if(lengthList == 0){
            messaggio = $("<p>Gli elementi sono stati tutti eliminati</p>");
            messaggio.css("color", "green");
            $("#secondList").append(messaggio);
        };
    });

    // form
    $("form select option").filter( ":selected" ).css({"border-color": "green", "background-color": "lightgreen"});

    $(":password").css({"background-color": "red", "color":"white"});

    $("form input").filter("[type='radio']").width("100px");

    $("#btnEmpty").on("click", function(){
        $("form").empty();
    })

    // create a list with .push() and an array
    $("#btnCreatelList").on("click", function(){
        var arrayLike = [];
        for(var i=1; i<11; i++){
            arrayLike.push("<p>" + i + "</p>");
            var newLinks = $("<a>Link-" + i + "</a>");
            arrayLike.push(newLinks);
            newLinks.attr("href", "file:///C:/DEV/jQuery_study/jquery_study/index.html");
        }
        $(document.body).append(arrayLike);
    });
});