// multiple events
$(function(){
    $("#btnMultipleEvents").on({
        "mouseenter": function(){ $(this).addClass("hovered");},
        "mouseleave": function(){ $(this).removeClass("hovered"); },
        "dblclick": function(){ $("#pMultipleEvents").addClass("selected");
        }} 
    );
    $("#pMultipleEvents").on({
        "mouseenter": function() { $(this).addClass("hovered"); },
        "mouseleave": function() { $(this).removeClass("hovered"); }
    });

    // .one() > run the event only once
    $("#oneBtn").one("click", function(){
        var newDiv = $("<div>nuovo div</div>");
        newDiv.addClass("divColorato")
        $(document.body).append(newDiv);
    });
});